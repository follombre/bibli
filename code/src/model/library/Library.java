package model.library;

import model.work.Novel;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Library implements Serializable {
    private transient PropertyChangeSupport support;

    private final List<Novel> novels;
    public static final String PROP_NOVELS = "PROP_NOVELS";

    public Library() {
        this.novels = new ArrayList<>();
    }

    public PropertyChangeSupport getSupport() {
        if (support == null) {
            support = new PropertyChangeSupport(this);
        }
        return support;
    }

    public List<Novel> getNovels() {
        return novels;
    }

    public void addNovel(Novel n) {
        novels.add(n);
        getSupport().fireIndexedPropertyChange(PROP_NOVELS, novels.size()-1, null, n);
    }

    public void removeNovel(Novel n) {
        int index = novels.indexOf(n);
        novels.remove(n);
        getSupport().fireIndexedPropertyChange(PROP_NOVELS, index, n, null);
    }

    public void addListener(PropertyChangeListener listener) {
        getSupport().addPropertyChangeListener(listener);
    }
}
